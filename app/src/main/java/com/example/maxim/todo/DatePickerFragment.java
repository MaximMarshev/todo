package com.example.maxim.todo;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.widget.DatePicker;

/**
 * Created by maxim on 16.08.17.
 */

@RequiresApi(api = Build.VERSION_CODES.N)
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private SimpleDateFormat dateFormatterEntry = new SimpleDateFormat("dd/mm/yyyy");
    private Calendar calendar;
    private DatePickedListener listener;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        listener = (DatePickedListener) getActivity();

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = String.valueOf(dayOfMonth) + "." + String.valueOf(month) + "." + String.valueOf(year);
        listener.onDatePicked(date);
    }

    public interface DatePickedListener {
        void onDatePicked(String date);
    }
}
