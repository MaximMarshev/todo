package com.example.maxim.todo;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by maxim on 09.08.17.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private ArrayList<Task> mDataset;
    //private Context context;
    private ViewHolder.ClickListener clickListener;
    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener,View.OnClickListener {
        // each data item is just a string in this case
        private TextView name;
        private TextView time;
        private TextView status;
        private TextView date;
        private CardView cardView;


        private ClickListener listener;

        public void bind(Task task)
        {
            name.setText(task.name);
            time.setText("Время:"+task.time);
            status.setText("Статус: Не выполнено");
            date.setText("Дата:"+task.date);
            cardView.setCardBackgroundColor(task.Priority);
        }



        public ViewHolder(View itemVeiw, ClickListener listener)
        {
            super(itemVeiw);
            name = (TextView)itemVeiw.findViewById(R.id.Name);
            time = (TextView)itemVeiw.findViewById(R.id.time);
            date = (TextView)itemVeiw.findViewById(R.id.date);
            status = (TextView)itemVeiw.findViewById(R.id.status);
            cardView = (CardView)itemVeiw.findViewById(R.id.card);

            this.listener = listener;

            itemVeiw.setOnClickListener(this);
            itemVeiw.setOnLongClickListener(this);
        }


        @Override
        public void onClick(View view) {
        }

        @Override
        public boolean onLongClick(View view) {
            if (listener != null) {
                return listener.onItemLongClicked(getAdapterPosition());
            }

            return false;
        }


        public interface ClickListener {
            boolean onItemLongClicked(int position);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(ArrayList<Task> myDataset,ViewHolder.ClickListener clickListener) {
        this.clickListener = clickListener;
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view, parent, false);
        // set the view's size, margins, paddings and layout parameter
        ViewHolder vh = new ViewHolder(v,clickListener);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.bind(mDataset.get(position));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }




}
