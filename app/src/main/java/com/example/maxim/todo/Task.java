package com.example.maxim.todo;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.widget.TextView;

/**
 * Created by maxim on 10.08.17.
 */

public class Task {

    public String name;
    public String date;
    public String time;
    public String status;
    public int Priority;
}
