package com.example.maxim.todo;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

import static android.R.attr.button;
import static android.R.attr.onClick;
import static android.R.attr.priority;
import static android.R.attr.timePickerDialogTheme;

public class MainActivity extends AppCompatActivity implements TimePickerFragment.TimePickedListener, DatePickerFragment.DatePickedListener, MyAdapter.ViewHolder.ClickListener{

    Button time;
    Button date;

    String[] priority = {"Высокий", "Средний", "Низкий"};
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<Task> myDataset = new ArrayList<Task>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mActionBarToolbar);

        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.floatingActionButton);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        //mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)


        mAdapter = new MyAdapter(myDataset,this);
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }


    public void addElement(View view)
    {


        final Task task = new Task();
        //Получаем вид с файла prompt.xml, который применим для диалогового окна:
        LayoutInflater li = LayoutInflater.from(this);
        View dialog = li.inflate(R.layout.dialog_task,null);

        //Создаем AlertDialog
        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(this);

        //Настраиваем prompt.xml для нашего AlertDialog:
        mDialogBuilder.setView(dialog);

        //Настраиваем отображение поля для ввода текста в открытом диалоге:
        final EditText name = (EditText)dialog.findViewById(R.id.name);
        time = (Button) dialog.findViewById(R.id.time);
        date = (Button)dialog.findViewById(R.id.date);


        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new TimePickerFragment();
                newFragment.show(getFragmentManager(), "timePicker");
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");
            }
        });

        //Создаем AlertDialog:
        final AlertDialog alertDialog = mDialogBuilder.create();


        Button add = (Button) dialog.findViewById(R.id.add);
        Button cancel = (Button) dialog.findViewById(R.id.cancel);

        final ConstraintLayout constraintLayout = (ConstraintLayout)dialog.findViewById(R.id.dialog);

        add.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                task.name = String.valueOf(name.getText());
                task.date = String.valueOf(date.getText());
                task.time = String.valueOf(time.getText());
                myDataset.add(task);
                alertDialog.dismiss();
            }

        });

       cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });


        Spinner spinner = (Spinner) dialog.findViewById(R.id.priority_spinner);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,priority);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        // выделяем элемент
        spinner.setSelection(2);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                switch(position)
                {
                    case 0:
                        constraintLayout.setBackgroundColor(getResources().getColor(R.color.High));
                        task.Priority = getResources().getColor(R.color.High);
                        break;
                    case 1:
                        constraintLayout.setBackgroundColor(getResources().getColor(R.color.Medium));
                        task.Priority = getResources().getColor(R.color.Medium);
                        break;
                    case 2:
                        constraintLayout.setBackgroundColor(getResources().getColor(R.color.Low));
                        task.Priority = getResources().getColor(R.color.Low);
                        break;

                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });




        alertDialog.show();
    }



    @Override
    public void onTimePicked(String mtime) {
        time.setText(mtime);
    }

    @Override
    public void onDatePicked(String date) {
        this.date.setText(date);
    }

    @Override
    public boolean onItemLongClicked(int position) {
        myDataset.remove(position);
        mRecyclerView.setAdapter(mAdapter);
        return true;
    }
}
